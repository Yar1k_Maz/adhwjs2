const books = [
  { author: "Люсі Фолі", name: "Список запрошених", price: 70 },
  { author: "Сюзанна Кларк", name: "Джонатан Стрейндж і м-р Норрелл" },
  { name: "Дизайн. Книга для недизайнерів.", price: 70 },
  { author: "Алан Мур", name: "Неономікон", price: 70 },
  { author: "Террі Пратчетт", name: "Рухомі картинки", price: 40 },
  { author: "Анґус Гайленд", name: "Коти в мистецтві" },
];
let ul = document.createElement("ul");
let root = document.querySelector(".root");
function bookItem(book) {
   let bookInfo = "";
   if (book.author) {
      bookInfo += `Author: ${book.author}, `;
   }
   if (book.name) {
      bookInfo += `Name: ${book.name}, `;
    }
    if (book.price) {
       bookInfo += `Price: ${book.price}`;
      }
  return bookInfo;
}
books.forEach((book) => {
  try {
    if (!book.author) {
      throw new Error(`Помилка немає Автора`);
   }
    if (!book.name) {
       throw new Error(`Помилка немає Назви`);
    }
    if (!book.price) {
       throw new Error(`Помилка немає Ціни`);
      }
      let li = document.createElement("li");
    li.innerText = bookItem(book);
    ul.appendChild(li);
   } catch (e) {
      console.error(e.message);
   }
});
root.append(ul);
